## Name

python-pptx-templating

## Description

The goal of this library is to add templating feature to python-pptx. So it is possible to create new slides & slide decks based on pre-populated master slides. Which is a very common case when you work hard with presentations.

## Installation

```bash
pip install python-pptx-templating
```

## Usage

The library leverages [mustache templates](https://en.wikipedia.org/wiki/Mustache_(template_system)).
To create a template, one needs to create a deck with master slides that have
a) so you can put your content into text and image placeholders when you provide them a special name
b) text placeholder's content contains template text

> manage shape names and order using [selection pane](https://support.microsoft.com/en-us/office/manage-objects-with-the-selection-pane-a6b2fd3e-d769-46c1-9b9c-b94e04a72550#OfficeVersion=macOS)

A couple of examples

1. a placeholder with a name {{my_placeholder}} will be populated with provided content: text or image. Image placeholder names end with `.image`, e.g. `Vacations.image`. So the content is replaced with an actual image that you provide a file path to. The image is scaled into the placeholder to without any distortion.
1. a placeholder with template content will be expanded based on provided variables

```text
This is my beautiful {{content}}
{{#good_weather}}It's sunny, yeah?{{/good_weather}}
```

This combination provides simple and powerful tool to create new presentations.

Here's a simple example of usage:

```python
template_deck = Presentation(pptx=Path("./test/resources/test_template.pptx"))

slide_master_name = "TEST_SLIDE_MASTER"
content = {
    "Title": "Sample Title",
    "Subtitle": "subtitle goes here",
    "TopContent": """Here goes top
multiline content.""",
    "MiddleContent": """middle
multiline content
...
...
some more lines
..""",
    "BottomContent": """bottom
line 1
line 2
line 3""",
    "Image.image": "./test/resources/python-logo@2x.png",
}

new_slide = add_slide(
    template_deck,
    slide_master_name,
    content,
)

```

## Authors and acknowledgment

Serhiy Masyutin

## License

MIT License
