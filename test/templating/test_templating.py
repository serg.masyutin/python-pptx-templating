import unittest
from pathlib import Path

from pptx import Presentation

from src.templating.templating import add_slide, find_shape_by_name


class TemplatingTestCase(unittest.TestCase):
    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)
        self.resources_path = Path("./test/resources/")

    def test_basic_add_slide(self):
        template_deck = Presentation(pptx=self.resources_path / "test_template.pptx")

        slide_master_name = "TEST_SLIDE_MASTER"

        content = {
            "Title": "Sample Title",
            "Subtitle": "subtitle goes here",
            "TopContent": """Here goes top
multiline content.""",
            "MiddleContent": """middle
multiline content
...
...
some more lines
..""",
            "BottomContent": """bottom
line 1
line 2
line 3""",
            "SideContent": """key1: value1a, value1b, value1c
key2: value2
key3: value3a, value3b""",
            "Image.image": str(self.resources_path / "python-logo@2x.png"),
        }

        new_slide = add_slide(
            template_deck,
            slide_master_name,
            content,
            # autofit_text_variable_names=["BottomContent"],
            # key_value_variable_names=["SideContent"]
        )

        # images are handled differently, drop the key before the check for now
        del content["Image.image"]

        for key in content.keys():
            expected_value = content[key]
            shape = find_shape_by_name(new_slide, f"{{{{{key}}}}}")
            self.assertTrue(shape.has_text_frame)

            value = get_text_from_shape(shape)
            self.assertEqual(expected_value, value)

    def test_expand_variables(self):
        template_deck = Presentation(pptx=self.resources_path / "test_template.pptx")

        slide_master_name = "TEST_SLIDE_MASTER"

        content = {
            "TopContent": """Here goes top
- level 1
- level 1 again
-- level 2
--- level 3
---- level 4
----- too much..
-- level 2 again""",
        }

        new_slide = add_slide(
            template_deck,
            slide_master_name,
            content,
        )

        expected_value = """Here goes top
level 1
level 1 again
level 2
level 3
level 4
too much..
level 2 again"""
        shape = find_shape_by_name(new_slide, "{{TopContent}}")
        self.assertTrue(shape.has_text_frame)

        value = get_text_from_shape(shape)
        self.assertEqual(expected_value, value)

    def test_format_paragraph(self):
        template_deck = Presentation(pptx=self.resources_path / "test_template.pptx")

        slide_master_name = "TEST_SLIDE_MASTER"

        content = {
            "TopContent": """Here goes top
multiline content with {{SomeVariable}}.""",
            "SomeVariable": "variable to expand is here",
        }

        new_slide = add_slide(
            template_deck,
            slide_master_name,
            content,
        )

        expected_value = """Here goes top
multiline content with variable to expand is here."""
        shape = find_shape_by_name(new_slide, "{{TopContent}}")
        self.assertTrue(shape.has_text_frame)

        value = get_text_from_shape(shape)
        self.assertEqual(expected_value, value)


def get_text_from_shape(shape) -> str:
    text_runs = []
    if shape.has_text_frame:
        for paragraph in shape.text_frame.paragraphs:
            for run in paragraph.runs:
                text_runs.append(run.text)

    return "\n".join(text_runs)


if __name__ == "__main__":
    unittest.main()
